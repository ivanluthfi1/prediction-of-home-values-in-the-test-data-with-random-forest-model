# Prediction of home values data with Random Forest model

The steps in this project are:

1. Build a Random Forest model with all of our data (X** and **y)
2. Read in the "test" data, which doesn't include values for the target. Predict home values in the test data with our Random Forest model.
3. Optionally, come back to see if we can improve our model by adding features or changing our model.
